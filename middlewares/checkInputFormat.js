const {isDateFormatValid} = require('../utils/common')

/** Check filter date format*/
function checkFilterDateFormat() {
    return async function (req, res, next) {
        console.info('=== CHECKING FILTER DATE FORMAT ===')
        if (req.query.filter && !isDateFormatValid(req.query.filter)) {
            res.status(501).send({
                error: 'Date format not valid'
            });
            return
        }
        next()
    }
}

module.exports = {
    checkFilterDateFormat,
}