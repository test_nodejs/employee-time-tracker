const {isDateFormatValid} = require('./common')

test('Check Date Format: expected true', () => {
    expect(isDateFormatValid("2023-09-26")).toBe(true)
})

test('Check Date Format: expected false', () => {
    expect(isDateFormatValid("09-06-2023")).toBe(false)
})

test('Check Date Format: expected false', () => {
    expect(isDateFormatValid("2023-09-26 09:52:14")).toBe(false)
})