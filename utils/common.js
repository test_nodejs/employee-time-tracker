require('datejs');

/** Check if the input string is a date in the format yyyy-MM-dd
 */
function isDateFormatValid(inputDate) {
    let parsedDate = Date.parseExact(inputDate, "yyyy-MM-dd");
    return (parsedDate !== null && parsedDate.toString("yyyy-MM-dd") === inputDate)
}

module.exports = {
    isDateFormatValid
}