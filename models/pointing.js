module.exports = (sequelize, DataTypes) => {
    const Pointing = sequelize.define("Pointing", {
        checkIn: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'check_in'
        },
        checkOut: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: null,
            field: 'check_out'
        },
        comment : {
            type : DataTypes.TEXT,
            allowNull: true
        },
        duration : {
            type : DataTypes.FLOAT,
            allowNull: true
        },
    },{
        tableName: 'pointings',
        createdAt: false,
        updatedAt: false
    });

    Pointing.associate = models => {
        // Pointing belongs to an employee
        Pointing.belongsTo(models.Employee,{
            foreignKey: {
                name: 'employee_id',
                field: 'employee_id',
                allowNull: false
            }
        })
    }

    return Pointing;
}