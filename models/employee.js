module.exports = (sequelize, DataTypes) => {
    const Employee = sequelize.define("Employee", {
        id: {
            type: DataTypes.STRING,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'name'
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'first_name'
        },
        dateCreated: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: new Date(),
            field: 'date_created'
        },
        department: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'department'
        },
    }, {
        tableName: 'employees',
        createdAt: false,
        updatedAt: false
    });

    Employee.associate = models => {
        Employee.hasMany(models.Pointing, {
            as: "pointings",
            foreignKey: {
                name: 'employee_id',
                field: 'employee_id',
                allowNull: false
            },
            onDelete: "cascade",
        })
    };

    return Employee;
}