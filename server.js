require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const helmet = require("helmet");
const db = require('./models')
const app = express();

// Use helmet to secure the app
app.use(helmet());
app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(bodyParser.urlencoded({extended: true}))

const employeeRouter = require('./routes/employeeRoutes');
const pointingRouter = require('./routes/pointingRoutes');

app.use('/api/employees', employeeRouter)
app.use('/api/pointings', pointingRouter)

//--------------------------/
//-----Swagger config-------/
//--------------------------/
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const {swaggerConfig} = require("./config/swaggerConfig");
// Swagger options
const swaggerOptions = swaggerConfig
const swaggerDocs = swaggerJsdoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

db.sequelize.sync({alter: false, force: false}).then(() => {
    app.listen(443);
    console.info("Server ready... Listening to port ", process.env.APP_PORT);
})