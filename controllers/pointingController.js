const {checkIn, checkOut, getEmployeePointings} = require("../services/pointingService");
exports.checkIn = async (req, res) => {
    try {
        console.info('*** IN CHECK IN ***')
        const {employeeId, comment} = req.body
        const insertedPointing = await checkIn(employeeId, comment)
        res.json(insertedPointing)
    } catch (err) {
        console.error('Error in check-in api : ' + err)
        res.status(500).send({
            success: false,
            error: 'Error in check-in api',
            error_message: err.message
        });
    }
}

exports.checkOut = async (req, res) => {
    try {
        console.info('*** IN CHECK OUT ***')
        const {employeeId, comment} = req.body
        const checkOutPointing = await checkOut(employeeId, comment)
        res.json(checkOutPointing)
    } catch (err) {
        console.error('Error in check-out api : ' + err)
        res.status(500).send({
            success: false,
            error: 'Error in check-out api',
            error_message: err.message
        });
    }
}

exports.getEmployeePointings = async (req, res) => {
    try {
        console.info('*** IN GET EMPLOYEE POINTINGS ***')
        const pointings = await getEmployeePointings(req.params.employeeId)
        res.json(pointings)
    } catch (err) {
        console.error('Error in get-employee-pointings api : ' + err)
        res.status(500).send({
            success: false,
            error: 'Error in get-employee-pointings api',
            error_message: err.message
        });
    }
}