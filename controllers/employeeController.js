const {addNewEmployee, getAllEmployees} = require('../services/employeeService')

exports.createEmployee = async (req, res) => {
    try {
        console.info('*** IN CREATE EMPLOYEE ***')
        const {name, firstName, department} = req.body
        const insertedEmployee = await addNewEmployee(name, firstName, department)
        res.json(insertedEmployee)
    } catch (err) {
        console.error('Error in create-employee api : ' + err)
        res.status(500).send({
            success: false,
            error: 'Error in create-employee api',
            error_message: err.message
        });
    }
}

exports.getAllEmployees = async (req, res) => {
    try {
        console.info('*** IN GET ALL EMPLOYEES ***')
        const employees = await getAllEmployees(req.query.filter)
        res.json(employees)
    } catch (err) {
        console.error('Error in get-all-employees api : ' + err)
        res.status(500).send({
            success: false,
            error: 'Error in get-all-employees api',
            error_message: err.message
        });
    }
}