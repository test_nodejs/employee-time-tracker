const express = require('express');
const router = express.Router();

router.use(function (req, res, next) {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', '*');
    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        res.send(200);
    } else {
        next();
    }
});

const employeeController = require('../controllers/employeeController');
const checkMiddleware = require('../middlewares/checkInputFormat')

/**
 * @swagger
 * /api/employees/new:
 *   post:
 *     summary: Create a new employee
 *     description: Create a new employee.
 *     tags: [Employees]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               firstName:
 *                 type: string
 *               department:
 *                 type: string
 *             example:
 *               name: Damien
 *               firstName: Michel
 *               department: Maths
 *     responses:
 *       200:
 *         description: Employee created successfully.
 *       500:
 *         description: An error occurred while creating the employee.
 */
router.post('/new', employeeController.createEmployee)

/**
 * @swagger
 * /api/employees:
 *   get:
 *     summary: Get all employees
 *     description: Get all employees, If we pass filter in query params => Get the employees created at this date.
 *     tags: [Employees]
 *     parameters:
 *       - in: query
 *         name: filter
 *         schema:
 *           type: string
 *         description: Add a date to filter employees by creation date (expl 2023-09-25)
 *     responses:
 *       200:
 *         description: A list of employees.
 *       500:
 *         description: An error occurred.
 */
router.get('/', checkMiddleware.checkFilterDateFormat(), employeeController.getAllEmployees)

module.exports = router;