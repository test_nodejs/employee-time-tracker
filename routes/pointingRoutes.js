const express = require('express');
const router = express.Router();

router.use(function (req, res, next) {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', '*');
    //intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        res.send(200);
    } else {
        next();
    }
});

const pointingController = require('../controllers/pointingController');

/**
 * @swagger
 * /api/pointings/check-in:
 *   post:
 *     summary: Employee Check-in
 *     description: Employee Check-in
 *     tags: [Pointings]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               employeeId:
 *                 type: string
 *               comment:
 *                 type: string
 *             example:
 *               employeeId: 5bfa8a60-5d35
 *               comment: "Arrived on time"
 *     responses:
 *       200:
 *         description: Check-in successful.
 *       500:
 *         description: An error occurred during check-in api.
 */
router.post('/check-in', pointingController.checkIn)
/**
 * @swagger
 * /api/pointings/check-out:
 *   post:
 *     summary: Employee Check-out
 *     description: Employee Check-out
 *     tags: [Pointings]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               employeeId:
 *                 type: string
 *               comment:
 *                 type: string
 *             example:
 *               employeeId: 5bfa8a60-5d35
 *               comment: "Arrived late"
 *     responses:
 *       200:
 *         description: Check-out successful.
 *       500:
 *         description: An error occurred during check-out api.
 */
router.post('/check-out', pointingController.checkOut)

/**
 * @swagger
 * /api/pointings/{employeeId}:
 *   get:
 *     summary: Get employee times
 *     description: Get the times (pointings) of an employee
 *     tags: [Pointings]
 *     parameters:
 *       - in: path
 *         name: employeeId
 *         required: true
 *         description: The ID of the employee to retrieve pointings for.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: A list of pointings for the employee.
 *       500:
 *         description: An error occurred while fetching pointings.
 */
router.get('/:employeeId', pointingController.getEmployeePointings)

module.exports = router;