# Used technologies and packages
This project was developed with NodeJs environment

`Express` to create the HTTP server

`Helmet` for the security of the APIs

`Sequelize ORM` to map the `MySql` database tables to JavaScript models

`Swagger` for the test and APIs documentation

`Jest` for unit tests

`Docker` for the containerization of the project

# Run on development environment

**1. Set the DB_PATH env var :**

In the `.env` file set the path where you want to save the database

Example: DB_PATH=/var/www/employee_time_tracker/dev/db_dev1

**2. Build the image and run the docker container :**

Execute the following command
~~~~
make run-dev
~~~~

The database will be automatically initialized, and the server is ready and listening
to port `5555` by default

**3. Test the APIs with your browser using the swagger documentation :**

In the browser open this link: http://localhost:5555/api-docs/

And you will be redirected to the swagger documentation interface

![img.png](img.png)

**4. Check the database with phpmyadmin :**

In the browser open this link to access to phpmyadmin:

http://localhost:8010/

Username: root

Password: YPA1koPLNe_i

**5. If you want to edit the code :**

Activate the nodemon module to reload automatically the container when coding:

Go to the file `Dockerfile`, comment the line `CMD ["npm","run", "prod"]` and 
uncomment the line `CMD ["npm","run", "dev"]`, then restart the container with the command
`make run-dev`

# Run on production environment
**1. Set the DB_PATH env var :**

In the `.env_prod` file set the path where you want to save the database

Example: DB_PATH=/var/www/employee_time_tracker/prod/db_prod1

**2. Build the image and run the docker container :**

Execute the following command
~~~~
make run-prod
~~~~

**3. Open container log :**

To get the logs of the container, execute the following command:

~~~~
docker logs -f prod_employee-time-tracker_employee-time-tracker-api_1
~~~~