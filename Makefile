run-dev:
	npm i
	docker network create employee-time-tracker-network-brahim || true
	docker-compose build
	docker-compose up

run-prod:
	docker network create employee-time-tracker-network-prod || true
	docker-compose -f docker-compose_prod.yml --env-file=.env_prod build
	docker-compose -f docker-compose_prod.yml --env-file=.env_prod up -d