const db = require("../models");

/** Check in */
async function checkIn(employeeId, comment) {
    try {
        // Check if the employee have to check-out first (have a pointing without check-out)
        let lastPointing = await db.Pointing.findOne({
            where: {
                employee_id: employeeId,
                checkOut: null
            }
        })
        if (lastPointing !== null) {
            console.error('Please Check-out first')
            throw new Error('Please Check-out first')
        }

        let newPointing = {
            comment: comment,
            employee_id: employeeId,
            checkIn: new Date(),
        }
        return await db.Pointing.create(newPointing)
    } catch (err) {
        console.error('Error in checkIn | ' + err.message)
        throw new Error('Error in checkIn | ' + err.message)
    }
}

/** Check out */
async function checkOut(employeeId, comment) {
    try {
        // Get the last check-in of the employee
        let lastPointing = await db.Pointing.findOne({
            where: {
                employee_id: employeeId,
                checkOut: null
            }
        })
        // Check If the employee does not check-in first
        if (lastPointing === null) {
            console.error('Please Check-in first')
            throw new Error('Please Check-in first')
        }
        const now = new Date()
        // Calculate the duration in minutes
        const duration  = Math.trunc((now - lastPointing.checkIn) / 60000)
        lastPointing.checkOut = now
        lastPointing.duration = duration
        lastPointing.comment = comment
        await lastPointing.save({
            fields: ['checkOut', 'duration', 'comment']
        })
        return lastPointing
    } catch (err) {
        console.error('Error in checkOut | ' + err.message)
        throw new Error('Error in checkOut | ' + err.message)
    }
}

async function getEmployeePointings(employeeId) {
    try {
        return await db.Pointing.findAll({
            where: {
                employee_id: employeeId
            }
        })
    } catch (err) {
        console.error('Error in getEmployeePointings | ' + err.message)
        throw new Error('Error in getEmployeePointings | ' + err.message)
    }
}

module.exports = {
    checkIn,
    checkOut,
    getEmployeePointings
}