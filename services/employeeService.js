const db = require("../models");
const {Op} = require("sequelize");

/** Create new employee */
async function addNewEmployee(name, firstName, department) {
    try {
        let newEmployee = {
            name: name,
            firstName: firstName,
            department: department,
        }
        return await db.Employee.create(newEmployee)

    } catch (err) {
        console.error('Error in addNewEmployee | ' + err.message)
        throw new Error('Error in addNewEmployee | ' + err.message)
    }
}

/** Get all employees
 * If we pass filter in query params => Get the employees created at this date*/
async function getAllEmployees(filter) {
    try {
        return await db.Employee.findAll({
            where: {
                [Op.and]: [
                    filter? {dateCreated: {[Op.startsWith]: filter}} : null
                ]
            }
        })
    } catch (err) {
        console.error('Error in getAllEmployees | ' + err.message)
        throw new Error('Error in getAllEmployees | ' + err.message)
    }
}

module.exports = {
    addNewEmployee,
    getAllEmployees
}