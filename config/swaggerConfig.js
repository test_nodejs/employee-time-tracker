const swaggerConfig = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Employee Time Tracker API Documentation',
      version: '1.0.0',
    },
  },
  host: 'localhost:' + process.env.APP_PORT,
  apis: ["./routes/*.js"],
};

module.exports = {
  swaggerConfig
}